package ar.edu.unsta.servicecourse.controller;

import ar.edu.unsta.servicecourse.entity.CoursesEntity;
import ar.edu.unsta.servicecourse.entity.Message;
import ar.edu.unsta.servicecourse.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/course")
@CrossOrigin("*")
public class CourseController {

    private final CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/list")
    public ResponseEntity<List<CoursesEntity>> getAll(){
        List<CoursesEntity> foundCourses = this.courseService.getAll();
        return new ResponseEntity<>(foundCourses, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public ResponseEntity<Message> save(@Valid @RequestBody CoursesEntity courseEntity, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity<>(new Message("Los campos ingresados son incorrectos"), HttpStatus.BAD_REQUEST);
        this.courseService.create(courseEntity);
        return new ResponseEntity<>(new Message("Curso guardado"), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CoursesEntity> get(@PathVariable String id) {
        try {
            CoursesEntity courseEntity = courseService.getCourse(id);
            return new ResponseEntity<CoursesEntity>(courseEntity, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<CoursesEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<CoursesEntity> update(@RequestBody CoursesEntity course, @PathVariable String id) {
        try {
            CoursesEntity existCourse = courseService.getCourse(id);
            course.setId(id);
            courseService.create(course);
            return new ResponseEntity<CoursesEntity>(existCourse,HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<CoursesEntity>(HttpStatus.NOT_FOUND);
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCourse(@PathVariable("id") String id) {
        try {
            courseService.deleteCourse(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}

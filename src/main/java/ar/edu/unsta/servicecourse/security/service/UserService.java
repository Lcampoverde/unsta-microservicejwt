package ar.edu.unsta.servicecourse.security.service;

import ar.edu.unsta.servicecourse.security.entity.User;
import ar.edu.unsta.servicecourse.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import java.util.Optional;

/***
 * Implementación de métodos de UsuarioService
 */
@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    public Optional<User> getByUserName(String userName){

        return userRepository.findByUserName(userName);
    }
    public boolean existByUserName(String userName){

        return userRepository.existsByUserName(userName);
    }
    public void save(User user){

        userRepository.save(user);
    }
}

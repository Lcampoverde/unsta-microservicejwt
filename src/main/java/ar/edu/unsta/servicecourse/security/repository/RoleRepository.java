package ar.edu.unsta.servicecourse.security.repository;

import ar.edu.unsta.servicecourse.security.entity.Role;
import ar.edu.unsta.servicecourse.security.enums.RoleList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/***
 * Interfaz que extiende de JpaRepository para manejar persistencias
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByRoleName(RoleList roleName);
}

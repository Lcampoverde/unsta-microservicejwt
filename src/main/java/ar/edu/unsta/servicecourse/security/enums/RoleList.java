package ar.edu.unsta.servicecourse.security.enums;

/***
 * Clase que contiene dos Rol Enum
 */
public enum RoleList {
    ROLE_ADMIN, ROLE_USER
}

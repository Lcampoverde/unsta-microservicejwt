package ar.edu.unsta.servicecourse.security.dto;
import javax.validation.constraints.NotBlank;

/**
 * Clase que hace de DTO para el login de usuario
 */
public class LoginUser {
    @NotBlank
    private String userName;
    @NotBlank
    private String password;
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}

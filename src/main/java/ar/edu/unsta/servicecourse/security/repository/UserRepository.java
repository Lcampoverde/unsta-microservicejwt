package ar.edu.unsta.servicecourse.security.repository;

import ar.edu.unsta.servicecourse.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

/***
 * Interfaz que extiende de JpaRepository para manejor persistencia.
 */
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByUserName(String userName);
    boolean existsByUserName(String userName);
}

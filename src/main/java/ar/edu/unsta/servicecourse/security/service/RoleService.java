package ar.edu.unsta.servicecourse.security.service;
import java.util.Optional;
import javax.transaction.Transactional;

import ar.edu.unsta.servicecourse.security.entity.Role;
import ar.edu.unsta.servicecourse.security.enums.RoleList;
import ar.edu.unsta.servicecourse.security.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/***
 * Implementación de métodos de RolRepository
 */
@Service
@Transactional
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    public Optional<Role> getByRoleName(RoleList roleName){
        return roleRepository.findByRoleName(roleName);
    }

}

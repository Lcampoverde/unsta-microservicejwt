package ar.edu.unsta.servicecourse.repository;

import ar.edu.unsta.servicecourse.entity.CoursesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface de manipulacion de datos
 *
 * @author Rene Palta / UNSTA
 * @version 0.1, 23/11/2022
 */
public interface CourseRepository extends JpaRepository<CoursesEntity, String>{
}

package ar.edu.unsta.servicecourse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicecourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicecourseApplication.class, args);
	}

}

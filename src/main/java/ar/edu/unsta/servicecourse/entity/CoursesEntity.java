package ar.edu.unsta.servicecourse.entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import org.hibernate.annotations.GenericGenerator;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import com.sun.istack.NotNull;

/**
 * Entidad que maneja los datos que se almacenan en BD
 * Se describe los campos de la BD
 * @author Rene Palta / UNSTA
 * @version 0.1, 23/11/2022
 */

@Entity
@Table(name = "course")
public class CoursesEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "nameCourse")
    @NotNull
    @NotEmpty(message="Nombre Obligatorio para ingresar un Curso")
    @Pattern(regexp = "[A-Za-z\\s]+", message="Caracter no valido para este campo")
    private String nameCourse;

    @Column(name = "descriptionCourse")
    @NotNull
    @NotEmpty(message="Descripcion Obligatorio para ingresar un Curso")
    @Pattern(regexp = "[A-Za-z\\s]+", message="Caracter no valido para este campo")
    private String descriptionCourse;

    @Column(name = "durationCourse")
    @NotNull
    //@NotEmpty(message="Duracion de curso es obligatorio")
    private int durationCourse;

    public CoursesEntity() {
    }

    public CoursesEntity(String id, String nameCourse, String descriptionCourse, int durationCourse) {
        this.id = id;
        this.nameCourse = nameCourse;
        this.descriptionCourse = descriptionCourse;
        this.durationCourse = durationCourse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public String getDescriptionCourse() {
        return descriptionCourse;
    }

    public void setDescriptionCourse(String descriptionCourse) {
        this.descriptionCourse = descriptionCourse;
    }

    public int getDurationCourse() {
        return durationCourse;
    }

    public void setDurationCourse(int durationCourse) {
        this.durationCourse = durationCourse;
    }
}

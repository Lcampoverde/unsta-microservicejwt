package ar.edu.unsta.servicecourse.service;

import ar.edu.unsta.servicecourse.entity.CoursesEntity;
import ar.edu.unsta.servicecourse.repository.CourseRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class CourseService {

    private final CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<CoursesEntity> getAll(){
        return  this.courseRepository.findAll();
    }

    public void create(CoursesEntity course){
        this.courseRepository.save(course);
    }

    public CoursesEntity getCourse(String id) {
        return this.courseRepository.findById(id).get();
    }

    public void deleteCourse(String id){
        courseRepository.deleteById(id);
    }
}

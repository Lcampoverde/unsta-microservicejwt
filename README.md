# TRABAJO FINAL UNSTA

_Microservicio manejo de Cursos, autenticacion y autorizacion_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋


```
jdk 1.8
```
```
spring framework 2.7.5
```
```
BD MYSQL
```
```
POSTMAN
```
```
MAVEN
```
### Deployment 🔧


```
git clone https://gitlab.com/grupodeestudioec/unsta-microservicejwt.git
```

```
cd unsta-microservicejwt
```

```
mvn clean install 
```

_Antes de ejecutar el microservicio, debes tener creado BD con el nombre **unstadb** y modificar el archivo application.properties_

```
spring.datasource.url= jdbc:mysql://localhost:3306/unstadb?useSLL=false

#username and password
spring.datasource.username = root
spring.datasource.password =
```
_Con el siguiente comando desplegaras el proyecto y automaticamente se realiza el mapeo de tablas y estas se crean en tu BD_

```
mvn spring-boot:run
```

_Las tablas creadas son las siguientes:_
```
- course
- role
- users
- user_role_relate
```

_Antes de comenzar a probar los servicios debes insertar los **ROLES**, el siguiente script debe ejecutarse en la tabla **role**_
```
INSERT INTO ROLE VALUES (1, 'ROLE_USER');
INSERT INTO ROLE VALUES (2, 'ROLE_ADMIN');
```


## Ejecutando las pruebas ⚙️

_Para ejecutar las pruebas usa el archivo json para importarlo en POSTMAN:_
```
UNSTA.json
```

_Debes seguir la siguiente secuencia:_
```
- Crear un usuario, si no especificas el ROLE, se asigna automaticamente un USER(ROLE) por defauld
- Loguearse con el usuario
- Para probar cada servicio debes colocar el token creado
```


## Autores ✒️



* **Diego Bordoli** 
* **Julian Contureyuzon** 
* **Luis Campoverde**
* **Mauricio Palacio**
* **Ruben Festini**
* **Rene Palta**

* ** 
UNSTA - Diciembre 2022







